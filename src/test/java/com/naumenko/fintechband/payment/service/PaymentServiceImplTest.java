package com.naumenko.fintechband.payment.service;

import com.naumenko.fintechband.payment.entity.Payment;
import com.naumenko.fintechband.payment.entity.PaymentStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.security.SecureRandom;

import static org.mockito.ArgumentMatchers.anyInt;

class PaymentServiceImplTest {

    private PaymentService paymentService;
    private SecureRandom randomMock;

    @BeforeEach
    void createPaymentService() {
        randomMock = Mockito.mock(SecureRandom.class);
        paymentService = new PaymentServiceImpl(randomMock);
    }

    @Test
    void getStatusByStringUuid_shouldThrowIllegalArgumentException_whenInputIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> paymentService.getStatusByStringUuid(null));
    }

    @Test
    void getStatusByStringUuid_shouldReturnPaymentWithStatus_whenInputIsNotNull() {
        Payment exceptedPayment = new Payment();
        exceptedPayment.setPaymentStatus(PaymentStatus.FAILED);
        exceptedPayment.setUuid("554e336f-fc94-4662-b748-83b763dfb76f");
        Mockito.when(randomMock.nextInt(3)).thenReturn(1);
        try (MockedStatic<PaymentStatus> paymentStatusMock = Mockito.mockStatic(PaymentStatus.class)) {
            paymentStatusMock.when(() -> PaymentStatus.intToEnum(anyInt())).thenReturn(PaymentStatus.FAILED);
            Assertions.assertEquals(exceptedPayment,
                paymentService.getStatusByStringUuid("554e336f-fc94-4662-b748-83b763dfb76f"));
        }
    }

    @Test
    void getStatusByStringUuid_shouldThrowIllegalArgumentException_whenRandomDigitIsNotFoundInEnum() {
        try (MockedStatic<PaymentStatus> paymentStatusMock = Mockito.mockStatic(PaymentStatus.class)) {
            paymentStatusMock.when(() -> PaymentStatus.intToEnum(anyInt())).thenThrow(IllegalArgumentException.class);
            Assertions.assertThrows(IllegalArgumentException.class, () ->
                paymentService.getStatusByStringUuid("554e336f-fc94-4662-b748-83b763dfb76f"));
        }
    }
}
