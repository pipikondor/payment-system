package com.naumenko.fintechband.payment.entity;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Getter
@Setter
public class Payment {

    @JsonView(Views.Uuid.class)
    private String uuid;

    @JsonView(Views.Status.class)
    private PaymentStatus paymentStatus;

    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotBlank(message = "Surname is mandatory")
    private String surname;

    @NotBlank(message = "middleName is mandatory")
    private String middleName;

    @Min(value = 1, message = "Sum should not be less than zero")
    private int paymentSum;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payment payment = (Payment) o;
        return paymentSum == payment.paymentSum && Objects.equals(uuid, payment.uuid) &&
            paymentStatus == payment.paymentStatus && Objects.equals(name, payment.name) &&
            Objects.equals(surname, payment.surname) && Objects.equals(middleName, payment.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, paymentStatus, name, surname, middleName, paymentSum);
    }
}
