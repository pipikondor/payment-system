package com.naumenko.fintechband.payment.entity;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum PaymentStatus {

    NEW(0), FAILED(1), DONE(2);

    private final int number;

    PaymentStatus(int number) {
        this.number = number;
    }

    public static PaymentStatus intToEnum(int digit) {
        return Arrays.stream(values()).filter(element -> element.number == digit).
            findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
