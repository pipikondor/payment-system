package com.naumenko.fintechband.payment.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorInfo {

    private String info;
}
