package com.naumenko.fintechband.payment.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.naumenko.fintechband.payment.entity.Payment;
import com.naumenko.fintechband.payment.entity.Views;
import com.naumenko.fintechband.payment.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/payments")
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping
    @JsonView(Views.Uuid.class)
    public Payment createPayment(@Valid @RequestBody Payment payment) {
        logger.info("POST /api/payments/");
        return paymentService.createPayment(payment);
    }

    @GetMapping("{uuid}/status")
    @JsonView(Views.Status.class)
    public Payment getPaymentStatus(@PathVariable String uuid) {
        logger.info("GET {}/status", uuid);
        return paymentService.getStatusByStringUuid(uuid);
    }
}

