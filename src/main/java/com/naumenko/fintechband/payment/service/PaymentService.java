package com.naumenko.fintechband.payment.service;

import com.naumenko.fintechband.payment.entity.Payment;

public interface PaymentService {

    Payment createPayment(Payment payment);

    Payment getStatusByStringUuid(String uuidAsString);
}
