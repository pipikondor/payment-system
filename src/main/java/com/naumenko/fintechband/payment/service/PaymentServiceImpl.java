package com.naumenko.fintechband.payment.service;

import com.naumenko.fintechband.payment.entity.Payment;
import com.naumenko.fintechband.payment.entity.PaymentStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);
    private final SecureRandom random;

    @Autowired
    public PaymentServiceImpl(SecureRandom random) {
        this.random = random;
    }

    @Override
    public Payment createPayment(Payment payment) {
        if (payment == null) {
            throw new IllegalArgumentException("Payment should not be null");
        }
        UUID uuid = UUID.randomUUID();
        payment.setUuid(uuid.toString());
        logger.info("random uuid - {} set to payment", uuid);
        return payment;
    }

    @Override
    public Payment getStatusByStringUuid(String uuidAsString) {
        if (uuidAsString == null) {
            throw new IllegalArgumentException("Uuid should not be null");
        }
        Payment payment = new Payment();
        payment.setUuid(uuidAsString);
        PaymentStatus paymentStatus = PaymentStatus.intToEnum(random.nextInt(3));
        payment.setPaymentStatus(paymentStatus);
        logger.info("random value {} set as enum {}", paymentStatus.getNumber(), paymentStatus);
        return payment;
    }
}
